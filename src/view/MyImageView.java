/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package view;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Toolkit;
import javax.swing.JPanel;

/**
 *
 * @author pedroluis
 */
public class MyImageView extends JPanel {
   private Image image;
   private boolean valid;
   public MyImageView() { 
       valid = false;
       setBackground(Color.WHITE);
       setAutoscrolls(true);
   }
   public void setImage(String url) {
       try {
           image = Toolkit.getDefaultToolkit().getImage(url);
           valid = true;
       } catch(Exception ex) {
           valid = false;
       }
       repaint();
   }
    @Override
   public void paint(Graphics g) {
        super.paint(g);
       if(valid) {
           g.drawImage(image, 0, 0, getWidth(), getHeight(), this);
       } else {
           g.drawString("No valid image." , getWidth()/2 - 28, getHeight()/2);
       }
   }

    public void setValid(boolean b) {
        valid = b;
        repaint();
    }
}
