/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package util;

/**
 *
 * @author pedroluis
 */
public class MyFunctions {
     public static int maxArray(int[] A, int Linf, int Lsup) {
        if( (Lsup - Linf) <= 1) {
            if(A[Linf] < A[Lsup]) {
               return A[Lsup];
            } else {
               return A[Linf];
            }
         } else {
            int m = (Linf + Lsup)/2;
            int max1 = maxArray(A, Linf, m);
            int max2 = maxArray(A, ( m + 1 ), Lsup);
            if(max1 > max2) {
                return max1;
            } else {
                return max2;
            }
        }
    }
}
