package ui;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.File;
import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.JButton;
import javax.swing.JCheckBoxMenuItem;
import javax.swing.JDialog;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JSplitPane;
import javax.swing.JToolBar;
import javax.swing.KeyStroke;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;
import javax.swing.filechooser.FileNameExtensionFilter;
import org.java.ayatana.ApplicationMenu;
import view.MyImageView;

/**
 *
 * @author pedroluis
 */
public class MainFrame extends JFrame implements ActionListener, PropertyChangeListener {

    private JToolBar toolBar;
    private JButton buttonLoadImage;
    private JButton buttonMad;
    private JFileChooser fileChooser;
    private JDialog imageChooser;
    private MyImageView imageView;
    private PanelImagen panelImagen_OR, panelImagen_PRO;
    private PanelHistogram panelHistogram;
    private MyImagePL apple;
    //
    private JMenuBar menuBar;
    private JMenu menuApp, menuOp;
    // -- menu app
    private JMenuItem miLoadImage, miQuit;
    // -- menu op
    private JCheckBoxMenuItem cbxmiShowBlue, cbxmiAnt;
    public MainFrame(){
        setSize(800, 600);
        panelImagen_OR = new PanelImagen();
        panelImagen_OR.setBorder(BorderFactory.createTitledBorder("Original"));
        panelImagen_PRO = new PanelImagen();
        panelImagen_PRO.setBorder(BorderFactory.createTitledBorder("Preprocesada"));
        panelHistogram = new PanelHistogram();
        panelHistogram.setBorder(BorderFactory.createTitledBorder("Histogram"));
        apple = new MyImagePL();
        
        initMenubar();
        initToolBar();
        initDialogImageView();
        
        Box boxLeft = Box.createVerticalBox();
        boxLeft.add(panelImagen_OR);
        boxLeft.add(panelImagen_PRO);
        boxLeft.setPreferredSize(new Dimension(300, 0));
        
        add(BorderLayout.NORTH, toolBar);
        add(BorderLayout.WEST, boxLeft);
        add(BorderLayout.CENTER, panelHistogram);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setLocationRelativeTo(null);
    }
    private void initMenubar() {
        // -- menu bar
        menuBar = new JMenuBar();
        
        menuApp = new JMenu("Application");
        menuApp.setMnemonic('A');
        miLoadImage = new JMenuItem("Load Image ...");
        miLoadImage.setActionCommand("load");
        miQuit = new JMenuItem("Quit");
        miQuit.setActionCommand("quit");
        miLoadImage.addActionListener(this);
        miLoadImage.setAccelerator(
                KeyStroke.getKeyStroke(KeyEvent.VK_L, InputEvent.CTRL_MASK));
        miQuit.addActionListener(this);
        miQuit.setAccelerator(
                KeyStroke.getKeyStroke(KeyEvent.VK_Q, InputEvent.CTRL_MASK));
        
        menuApp.add(miLoadImage); menuApp.addSeparator();
        menuApp.add(miQuit);
        
        menuOp = new JMenu("Options");
        menuOp.setMnemonic('O');
        cbxmiAnt = new JCheckBoxMenuItem("Antialiasing");
        cbxmiAnt.setActionCommand("ant");
        cbxmiShowBlue = new JCheckBoxMenuItem("Show blue");
        cbxmiShowBlue.setActionCommand("showblue");
        
        cbxmiAnt.addActionListener(this);
        cbxmiShowBlue.addActionListener(this);
        
        menuOp.add(cbxmiAnt);  menuOp.addSeparator();
        menuOp.add(cbxmiShowBlue);        
        
        menuBar.add(menuApp); menuBar.add(menuOp);
        setJMenuBar(menuBar);
        //
    }
    private void initToolBar() {
        toolBar = new JToolBar();
        buttonLoadImage = new JButton("Load Image ...");
        buttonLoadImage.setActionCommand("load");
        buttonLoadImage.addActionListener(this);
        
        buttonMad = new JButton("Madurez");
        buttonMad.setEnabled(false);
        buttonMad.setActionCommand("mad");
        buttonMad.addActionListener(this);
        toolBar.add(buttonLoadImage);  toolBar.addSeparator();
        toolBar.add(buttonMad); 
    }
    private void initDialogImageView() {
        imageView = new MyImageView();
        imageView.setPreferredSize(new Dimension(300, 200));
        // image chooser
        fileChooser = new JFileChooser();
        fileChooser.setCurrentDirectory(new File(System.getProperty("user.home")));
        fileChooser.removeChoosableFileFilter(fileChooser.getFileFilter());
        fileChooser.setFileFilter(
                new FileNameExtensionFilter("Images(*.jpg *.jpeg *.bmp)", "jpg", "jpeg", "bmp"));
        fileChooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
        fileChooser.addActionListener(this);
        fileChooser.addPropertyChangeListener("SelectedFileChangedProperty", this);

        imageChooser = new JDialog(this);
        imageChooser.setTitle("Open to Image");
        JSplitPane split = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT);
        imageChooser.setContentPane(split);
        split.add(fileChooser); split.add(imageView); 
        imageChooser.pack();
        imageChooser.setLocationRelativeTo(this);
    }
    @Override
    public void actionPerformed(ActionEvent ae) {
        String action = ae.getActionCommand();
        if(action.compareTo("load") == 0) {
            imageChooser.show();
        }else if(action.compareTo("ApproveSelection") == 0) {
            String strFile = fileChooser.getSelectedFile().getAbsolutePath();
            if(!apple.load(strFile)) {
                JOptionPane.showMessageDialog(this, apple.lastErrorMessage());
                panelImagen_OR.setMyAppleImage(null);
                panelImagen_PRO.setMyAppleImage(null);
                panelHistogram.setHistogram(null);
                buttonMad.setEnabled(false);
                return;
            }
            panelImagen_OR.setMyAppleImage(apple.getImageOr());
            panelImagen_PRO.setMyAppleImage(apple.getImage());
            panelHistogram.setHistogram(apple.getHistogramRGB());
            buttonMad.setEnabled(true);
            imageChooser.show(false);
        } else if(action.compareTo("mad") == 0) {
            int m = apple.analyzedMaxR();
            if(m == 1) {
                JOptionPane.showMessageDialog(this, "Manzana madura");
            } else if(m == 0) {
                JOptionPane.showMessageDialog(this, "Manzana verde");
            } else {
                JOptionPane.showMessageDialog(this, "Manzana en otra etapa");
            }
        } else if(action.compareTo("CancelSelection") == 0) {
            imageChooser.show(false);
        } else if(action.compareTo("quit") == 0) {
            System.exit(0);
        } else if(action.compareTo("ant") == 0) {
            if(cbxmiAnt.isSelected()) {
                panelHistogram.setShowAntialiasing(true);
                panelHistogram.repaint();
            } else {
                panelHistogram.setShowAntialiasing(false);
                panelHistogram.repaint();
            }
        } else if(action.compareTo("showblue") == 0) {
            if(cbxmiShowBlue.isSelected()) {
                panelHistogram.setShowBlue(true);
                panelHistogram.repaint();
            } else {
                panelHistogram.setShowBlue(false);
                panelHistogram.repaint();
            }
        } 
    }

    @Override
    public void propertyChange(PropertyChangeEvent pce) {
        if(pce.getNewValue() != null) {
            imageView.setImage(pce.getNewValue().toString());
        } else {
           imageView.setValid(false); 
        }
    }
    
    public static void main(String[] args) {
        
        try{
            for(UIManager.LookAndFeelInfo info :
                    UIManager.getInstalledLookAndFeels()){
                //System.out.println(info.getName());
                if(info.getName().compareTo("Metal") == 0){
                    UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            } // Metal Nimbus CDE/Motif GTK+
        }catch(ClassNotFoundException | InstantiationException | 
                IllegalAccessException | UnsupportedLookAndFeelException ex) {
            JOptionPane.showMessageDialog(null,
                    "Error al cargar Tema \n"+ex.getMessage(),
                    "Load Theme",
                    JOptionPane.WARNING_MESSAGE);
        }
        
        MainFrame ia = new MainFrame();
        ia.setTitle("Red Apple Maturity");
        ia.setVisible(true);
        
        ApplicationMenu.tryInstall(ia);
    }
}
