/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ui;

import java.awt.image.BufferedImage;
import java.awt.image.WritableRaster;
import java.io.File;
import java.io.IOException;
import javax.imageio.ImageIO;

/**
 *
 * @author pedroluis
 */
public class MyImagePL {

    private BufferedImage bufferedImage;
    private BufferedImage imageAux;
    private String errorMessage;
    private int[][] redMatrix;
    private int[][] greenMatrix;
    private int[][] blueMatrix;
    private boolean h;
    // 
    int[] histogramRGB;

    public MyImagePL() {
        h = false;
    }
    
    public boolean load(String file_url) {
        h = false;
        try {
            bufferedImage = ImageIO.read(new File(file_url));
            imageAux = ImageIO.read(new File(file_url));
            redMatrix = new int[bufferedImage.getWidth()][bufferedImage.getHeight()];
            greenMatrix = new int[bufferedImage.getWidth()][bufferedImage.getHeight()];
            blueMatrix = new int[bufferedImage.getWidth()][bufferedImage.getHeight()];
            WritableRaster raster = bufferedImage.getRaster();
            
            if(raster.getNumBands() != 3) {
                errorMessage = "Not in rgb image";
                return false;
            }
            int r, g, b;
            for(int i = 0; i < bufferedImage.getWidth(); i++) {
                for (int j = 0; j < bufferedImage.getHeight(); j++) {
                    r = raster.getSample(i, j, 0);
                    g = raster.getSample(i, j, 1);
                    b = raster.getSample(i, j, 2);
                    
                    if(r>= 250 && g >= 250 && b >= 250) {
                        r = 0; g = 0; b = 255;
                        raster.setPixel(i, j, new int[]{0, 0, 0});
                    }
                    
                    redMatrix[i][j]   = r;
                    greenMatrix[i][j] = g;
                    blueMatrix[i][j]  = b;
                }
            }
        } catch (IOException ex) {
            errorMessage = ex.getMessage();
            return false;
        }
        return true;
    }
    public int[] getHistogramRGB() {
        if(h) {
            return histogramRGB;
        }
        int len = 768;
        histogramRGB = new int[len];
        for (int i = 0; i < len; i++) {
            histogramRGB[i] = 0;
        }
        for (int i = 0; i < bufferedImage.getWidth(); i++) {
            for (int j = 0; j < bufferedImage.getHeight(); j++) {
                histogramRGB[ redMatrix[i][j] ] += 1;
                histogramRGB[ greenMatrix[i][j] + 256 ] += 1;  
                histogramRGB[ greenMatrix[i][j] + 512 ] += 1;
            }
        }
        h = true;
        return histogramRGB;
    }
    //
    public BufferedImage getImageOr() {
        return imageAux;
    }
    public BufferedImage getImage() {
        return bufferedImage;
    }
    public String lastErrorMessage() {
        return errorMessage;
    }
    public int[][] getRedMatrix(){
        return redMatrix;
    }
    public int[][] getGreenMatrix(){
        return greenMatrix;
    }
    //
    public int analyzedMaxR() {
        getHistogramRGB();
        int len = 64;
        int i = len, j = 256 + len;
        double maxR = 0, maxG = 0; double d;
        while(i < 256) {
            maxR += histogramRGB[i]; 
            maxG += histogramRGB[j]; 
            i++; j++;
        }
        if(maxR > maxG) {
            double p = ((maxG * 100 ) / maxR);
            double p_v = 50.0;
            if(p >= 99.3 && p <= 99.7) {
                return 0;
            } else if(p < p_v) {
                return 1;
            } else {
                return -1;
            }
        } else {
            return 0;
        }        
    }//
}
