/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ui;

import java.awt.Graphics;
import java.awt.image.BufferedImage;
import javax.swing.JPanel;

/**
 *
 * @author pedroluis
 */
public class PanelImagen extends JPanel {
    private BufferedImage image;
    public PanelImagen() {
        this.image = null;
        //this.setBackground(Color.white);
    }
    public PanelImagen(BufferedImage image) {
       this.image = image;
    }
    public void setMyAppleImage(BufferedImage image) {
       this.image = image;
       repaint();
    }
    @Override
    public void paint(Graphics g) {
        super.paint(g);
        if(image != null) { 
            g.drawImage(image, 20, 20, getWidth() - 40, getHeight() - 40, this);
        }
    }
}
