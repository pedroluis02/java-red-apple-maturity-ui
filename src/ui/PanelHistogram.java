
package ui;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.RenderingHints;
import java.awt.geom.Line2D;
import java.awt.geom.RoundRectangle2D;
import javax.swing.JPanel;
import util.MyFunctions;

/**
 * @author pedroluis
 */

public class PanelHistogram extends JPanel {
    private boolean hBlue;
    private boolean hAntialiasing;
    private int []histogram;

    public PanelHistogram() {
        this.histogram = null;
        this.hBlue = false;
        this.hAntialiasing = false;
        this.setBackground(Color.WHITE);
    }
    public PanelHistogram(int []histogram, boolean hBlue) {
        this.histogram = histogram;
        this.hBlue = hBlue;
        this.hAntialiasing = false;
        this.setBackground(Color.WHITE);
    }
    // show blue
    public boolean isShowBlue() {
        return hBlue;
    }
    public void setShowBlue(boolean hBlue) {
        this.hBlue = hBlue;
    }
    public void setShowAntialiasing(boolean hAntialiasing) {
        this.hAntialiasing = hAntialiasing;
    }
    public void setHistogram(int []histogram) {
        this.histogram = histogram;
        repaint();
    }
    //
    @Override
    public void paint(Graphics g) {
        super.paint(g);
        Graphics2D g2 = (Graphics2D)g;
        if(hAntialiasing) {
            g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
                RenderingHints.VALUE_ANTIALIAS_ON);
        }
        if(histogram != null) {
            drawHistogramRGB(g2);
        }
    }
    //
     private void drawHistogramRGB(Graphics2D g) {
        for(int i = 0; i<= 5; i++) {
            histogram[i] = 0;
            histogram[i + 256] = 0;
            if(hBlue) {
                histogram[i + 512] = 0;
            }
        }
        
        double centerX = 60,
               centerY = getHeight() - 20;
        int top = 40;
        
        double lengthX = Point.distance(centerX, centerY, getWidth() - 20, centerY),
               lengthY = Point.distance(centerX, centerY, centerX, top);
        
        double len; 
        if(hBlue) {
            len = histogram.length;
        } else {
            len = 512;
        }
        
        double maxX = lengthX / len, 
               maxY = MyFunctions.maxArray(histogram, 0, (int)len - 1),
               xa = centerX + 2, ya = centerY - 2,
               x, y, xex = xa;
        Color color; int it = 0;
        
        
        g.setColor(Color.BLUE);
        
        g.draw(new Line2D.Double(centerX, centerY, centerX, top));
        g.draw(new Line2D.Double(centerX, centerY, getWidth() - 20, centerY));   
        
        int i = 0;
        while(i < len) {
            x = xex;
            xex = x + maxX;
            y = (histogram[i] * lengthY) / maxY;
            
            if(i <= 255) {
                color = new Color(it, 0, 0);
            } else if(i <= 511) {
                color = new Color(0, it, 0);
            } else {
                color = new Color(0, 0, it);
            }

            if(y > 0.0) {
                g.setColor(color);
                g.fill(new RoundRectangle2D.Double(x, ya - y, maxX, y, 5, 5));
            }

            if (it == 255  || it == 511) {
                it = -1;
            }
            it++;  i++;
        }
        
        g.setColor(Color.BLUE);
        g.drawString("0", (int)centerX - 15, (int)centerY);
        g.setColor(Color.RED);
        g.drawString((int)maxY+"", (int)centerX - 20, top - 10);
//        g.setColor(Color.BLUE);
//        
//        double d = maxY / 100,
//               f = Math.floor(lengthY / d);
//        int p = (int)centerY; int m = 0;
//        float punteo[] = new float[]{5f, 5f};
//        for(int k = 100; k < maxY; k += 100) {
//           p -= f;
//           g.drawString("" + k, (int)centerX - 40, p); 
//           g.setStroke(new BasicStroke(0.2f, 
//                    BasicStroke.CAP_ROUND, BasicStroke.JOIN_MITER,
//                    5f, punteo, 2f));
//           g.draw(new Line2D.Double(centerX, p, getWidth() - 20, p));
//           g.setStroke(new BasicStroke(1));
//        }
    }
}
